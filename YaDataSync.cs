﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Greed.Core
{
    public static class YaDataSync
    {
        private const string _Key = "197390a3f82d4a7490ea40551de25221";
        private const string _Token = "AQAAAAAjMhGyAAU4HGfN75LKlUbJhJo--OYsJik";

        private static string Ajax(string url, string method = "GET", string data = null, List<string> headers = null)
        {
            WebResponse response;
            return Ajax(url, out response, method, data, headers);
        }

        private static string Ajax(
            string url, out WebResponse response, string method = "GET", string data = null, List<string> headers = null
        )
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.Headers.Add("Authorization: OAuth " + _Token);

            if (headers != null) headers.ForEach(req.Headers.Add);

            req.ContentType = "application/json";

            req.Method = method;

            if (data != null)
            {
                byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(data);
                req.ContentLength = byteArray.Length;

                //записываем данные в поток запроса
                using (Stream dataStream = req.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }
            }

            try
            {
                response = req.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream());
                return sr.ReadToEnd();
            }
            catch (Exception)
            {
            }

            response = null;
            return null;
        }

        /// <summary>
        /// Получение списка баз данных
        /// https://tech.yandex.ru/datasync/http/doc/tasks/get-databases-docpage/
        /// </summary>
        /// <param name="context">Контекст базы данных (app/user) https://tech.yandex.ru/datasync/http/doc/dg/concepts/data-structure-docpage/#data-structure__context </param>
        /// <param name="fields">Список атрибутов базы данных, которые будут содержаться в ответе. Атрибуты перечисляются через запятую. (null) https://tech.yandex.ru/datasync/http/doc/dg/concepts/data-structure-docpage/ </param>
        /// <param name="limit">Количество ресурсов (баз данных) в выдаче (null)</param>
        /// <param name="offset">Смещение от начала списков ресурсов (null)</param>
        public static YaDBListInfo GetDBListInfo(string context = "app", string fields = null, int? limit = null, int? offset = null)
        {
            var query = "";

            if (fields != null)
            {
                query += "fields=" + fields;
            }

            if (limit != null)
            {
                if (query != "") query += "&";
                query += "limit=" + limit;
            }

            if (offset != null)
            {
                if (query != "") query += "&";
                query += "offset=" + offset;
            }

            return JsonConvert.DeserializeObject<YaDBListInfo>(
                Ajax("https://cloud-api.yandex.net/v1/data/"+ context + "/databases/" + (query == "" ? "" : "?" + query))
            );
        }

        /// <summary>
        /// Получение снапшота базы данных
        /// https://tech.yandex.ru/datasync/http/doc/tasks/get-snapshot-docpage/
        /// </summary>
        /// <param name="database_id">Идентификатор базы данных.</param>
        /// <param name="context">Контекст базы данных (app/user) https://tech.yandex.ru/datasync/http/doc/dg/concepts/data-structure-docpage/#data-structure__context </param>
        /// <param name="fields">Список атрибутов базы данных, которые будут содержаться в ответе. Атрибуты перечисляются через запятую. (null) https://tech.yandex.ru/datasync/http/doc/dg/concepts/data-structure-docpage/ </param>
        /// <param name="collection_id">Идентификаторы коллекций, по которым необходимо фильтровать записи базы данных. Коллекции перечисляются через запятую. (null)</param>
        public static YaDBSnapshot GetSnapshot(string database_id, string context = "app", string fields = null, string collection_id = null)
        {
            var query = "";

            if (fields != null)
            {
                query += "fields=" + fields;
            }

            if (collection_id != null)
            {
                if (query != "") query += "&";
                query += "limit=" + collection_id;
            }

            return JsonConvert.DeserializeObject<YaDBSnapshot>(
                Ajax("https://cloud-api.yandex.net/v1/data/" + context + "/databases/" + database_id + "/snapshot/"
                    + (query == "" ? "" : "?" + query))
            );
        }

        /// <summary>
        /// Отправка изменений
        /// https://tech.yandex.ru/datasync/http/doc/tasks/add-changes-docpage/
        /// </summary>
        public static YaDBSnapshot SendChange(YaDBSnapshot database, YaChange change, string context = "app", string fields = null)
        {
            //https://cloud-api.yandex.net/v1/data/<context>/databases/<database_id>/deltas/[?fields=<значение>]
            var query = "";

            if (fields != null)
            {
                query += "fields=" + fields;
            }

            WebResponse response;

            Ajax(
                "https://cloud-api.yandex.net/v1/data/" + context + "/databases/" + database.database_id + "/deltas/"
                    + (query == "" ? "" : "?" + query),
                out response,
                "POST",
                JsonConvert.SerializeObject(change),
                new List<string> { "If-Match: " + database.revision }
            );

            database.revision = Convert.ToInt32(response.Headers["ETag"]);

            return database;
        }
    }
}