﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Greed.Core
{
    public class ScreenArr
    {
        public byte[] body;
        public Size size;
    }

    public class ImgLib
	{
        public static ScreenArr GetScreenArr(int hwnd)
        {
            try
            {
                var rect = Hooker.GetHwndRect(hwnd);
                return new ScreenArr { body = Hooker.GetRectScreenArr(rect), size = rect.Size };
            }
            catch (Exception)
            {
                GC.Collect();
                return GetScreenArr(hwnd);
            }
        }

        public static ScreenArr CutScreenArr(ScreenArr screen, int x = 0, int y = 0, int w = 10, int h = 10)
        {
            var ow = screen.size.Width;
            var body = screen.body;
            var result = new byte[w * h * 4];
            var a = 0;
            for (var dy = 0; dy < h; dy++)
            {
                for (var dx = 0; dx < w; dx++)
                {
                    var j = (y + dy) * ow * 4 + (x + dx) * 4;
                    for (int i = 0; i < 4; i++)
                    {
                        result[a++] = body[j + i];
                    }
                }
            }

            return new ScreenArr { body = result, size = new Size(w, h) };
        }

        public static Bitmap ScreenArrToBitmap(ScreenArr screen)
        {
            var bmp = new Bitmap(screen.size.Width, screen.size.Height);

            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.WriteOnly, bmp.PixelFormat);

            IntPtr ptr = bmpData.Scan0;

            int bytes = bmpData.Stride * bmp.Height;

            Marshal.Copy(screen.body, 0, ptr, bytes);

            bmp.UnlockBits(bmpData);

            return bmp;
        }

        public static void DebugImg(ScreenArr screen, int x = 0, int y = 0, int w = 10, int h = 10)
        {
            ScreenArrToBitmap(CutScreenArr(screen, x, y, w, h))
                .Save(DateTime.Now.ToString().Replace(":", "_") + "_" + DateTime.Now.Millisecond + ".png", ImageFormat.Png);
        }

        public static void DebugImg(int ow, byte[] arr, int x = 0, int y = 0, int w = 10, int h = 10)
        {
            var bmp = new Bitmap(w, h);
            for (var dy = 0; dy < h; dy++)
            {
                for (var dx = 0; dx < w; dx++)
                {
                    var j = (y + dy) * ow * 4 + (x + dx) * 4;
                    bmp.SetPixel(dx, dy, Color.FromArgb(arr[j + 2], arr[j + 1], arr[j]));
                }
            }
            bmp.Save(DateTime.Now.ToString().Replace(":", "_") + "_" + DateTime.Now.Millisecond + ".png", ImageFormat.Png);
        }
    }
}