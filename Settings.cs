﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;

namespace Greed.Core
{
	public static class Settings
	{
		public const string FormParamsFileName = "FormParams.xml";
		public const string SettingsFileName = "GreedSettings.json";
		
        private static Dictionary<string, string> _Body = new Dictionary<string,string>();
		private static bool _isLoad;
        private static bool _isInited = false;

		public static void Load()
		{
			_isLoad = true;

            if (File.Exists(SettingsFileName))
            {
                try {
                    _Body = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(SettingsFileName));
                } catch {

                }
            }

			_isLoad = false;
            _isInited = true;
		}

        public static void WriteFile(string path, object obj)
        {
            File.WriteAllText(path, JsonConvert.SerializeObject(obj, Formatting.Indented));
        }

        public static T ReadFile<T>(string path, T defValue)
        {
            if (File.Exists(path))
            {
                try
                {
                    return JsonConvert.DeserializeObject<T>(File.ReadAllText(path));
                }
                catch {}
            }

            return defValue;
        }

        public static void Save()
		{
			if (_isLoad) return;
            WriteFile(SettingsFileName, _Body);
		}

        public static T Get<T>(string key, T defValue)
        {
            if (!_isInited) Load();
            if (!_Body.ContainsKey(key))
            {
                return defValue;
            }
            return JsonConvert.DeserializeObject<T>(_Body[key]);
        }

        public static void Set(string key, object value)
        {
            _Body[key] = JsonConvert.SerializeObject(value);
            Save();
        }

        public static FormParams GetDeltaMain(FormParams fp, Form MainForm)
        {
            var mf = MainForm;
            fp.X -= mf.Left;
            fp.Y -= mf.Top;
            return fp;
        }

        public static void SetDeltaMain(FormParams fp, Form MainForm)
        {
            var mf = MainForm;
            fp.X += mf.Left;
            fp.Y += mf.Top;
        }
	}

	public class FormParams
	{
		public int X;
		public int Y;
		public int W;
		public int H;

		public static FormParams Get(Form form, Form mainForm, Func<FormParams, Form, FormParams> fn = null)
		{
            var res = new FormParams
            {
                X = form.Left,
                Y = form.Top,
                W = form.Width,
                H = form.Height
            };

            return fn == null ? res : fn(res, mainForm);
		}

		public void Set(Form form, Form mainForm, Action<FormParams, Form> fn = null)
		{
            if (fn != null) fn(this, mainForm);
			form.Left = X;
			form.Top = Y;
			form.Width = W;
			form.Height = H;
		}

		public static FormParams Load(string name)
		{
			return Serialization.DeserializeFromFile<FormParams>(name + "_" + Settings.FormParamsFileName);
		}

		public void Save(string name)
		{
			this.SerializeToFile(name + "_" + Settings.FormParamsFileName);
		}
	}
}