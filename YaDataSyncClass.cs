﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Greed.Core
{
    public class YaDBInfo
    {
        /// <summary>
        /// Уникальный идентификатор базы, который генерирует сервер. При удалении и создании базы этот идентификатор изменяется.
        /// </summary>
        public string handle;

        /// <summary>
        /// Количество записей в базе данных.
        /// </summary>
        public int records_count;

        /// <summary>
        /// Дата и время создания базы данных.
        /// </summary>
        public DateTime created;

        /// <summary>
        /// Дата и время последней модификации базы данных.
        /// </summary>
        public DateTime modified;

        /// <summary>
        /// Идентификатор базы данных.
        /// </summary>
        public string database_id;

        /// <summary>
        /// Номер текущей ревизии базы данных.
        /// </summary>
        public int revision;

        /// <summary>
        /// Размер базы данных в байтах.
        /// </summary>
        public int size;
    }

    public class YaDBListInfo
    {
        /// <summary>
        /// Массив объектов, содержащих информацию о существующих базах данных.
        /// </summary>
        public List<YaDBInfo> items;

        /// <summary>
        /// Общее количество элементов в списке выдачи.
        /// </summary>
        public int total;

        /// <summary>
        /// Количество элементов на странице выдачи.
        /// </summary>
        public int limit;

        /// <summary>
        /// Смещение от начала списка выдачи.
        /// </summary>
        public int offset;
    }

    /// <summary>
    /// https://tech.yandex.ru/datasync/http/doc/tasks/create-changes-docpage/#create-changes__data-types
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class YaValue
    {
        [JsonProperty]
        public string type;

        [JsonProperty("string", NullValueHandling = NullValueHandling.Ignore)]
        public string String;

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? integer;

        [JsonProperty("double", NullValueHandling = NullValueHandling.Ignore)]
        public double? Double;

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? datetime;

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<YaValue> list;
    }

    public class YaField
    {
        /// <summary>
        /// Идентификатор поля.
        /// </summary>
        public string field_id;

        /// <summary>
        /// Новое значение поля.
        /// </summary>
        public YaValue value;
    }

    public class YaRecord
    {
        /// <summary>
        /// Идентификатор записи, к которой применялось изменение.
        /// </summary>
        public string record_id;
        /// <summary>
        /// Идентификатор коллекции, которой принадлежит запись.
        /// </summary>
        public string collection_id;

        /// <summary>
        /// Список полей записи.
        /// </summary>
        public List<YaField> fields;

        /// <summary>
        /// Номер ревизии, который был присвоен базе данных после применения соответствующих изменений.
        /// </summary>
        public int revision;
    }

    public class YaRecords
    {
        public List<YaRecord> items;
    }

    public class YaDBSnapshot
    {
        /// <summary>
        /// Количество записей в базе данных.
        /// </summary>
        public int records_count;

        /// <summary>
        /// Дата и время создания.
        /// </summary>
        public DateTime created;
        /// <summary>
        /// Дата и время последней модификации.
        /// </summary>
        public DateTime modified;

        /// <summary>
        /// Записи
        /// </summary>
        public YaRecords records;

        /// <summary>
        /// Идентификатор базы данных.
        /// </summary>
        public string database_id;

        /// <summary>
        /// Номер текущей ревизии базы данных.
        /// </summary>
        public int revision;
        /// <summary>
        /// Размер базы данных в байтах.
        /// </summary>
        public int size;
    }

    public class YaChange
    {
        /// <summary>
        /// Поясняющий комментарий к изменению.
        /// </summary>
        public string delta_id;
        /// <summary>
        /// Изменения отдельных записей базы данных.
        /// </summary>
        public List<YaRecordChange> changes;
    }

    public class YaRecordChange
    {
        /// <summary>
        /// Тип изменения, применяемого к записи. Подробнее см. в Типы изменений. https://tech.yandex.ru/datasync/http/doc/tasks/create-changes-docpage/#create-changes__changes-types
        /// </summary>
        public string change_type;

        /// <summary>
        /// Идентификатор коллекции, которой принадлежит запись. Представляет собой строку длиной от 1 до 128 символов из набора [a-z, A-Z, 0-9, _, -, .]. Может начинаться с любого символа из набора, кроме точки.
        /// </summary>
        public string collection_id;

        /// <summary>
        /// Идентификатор записи. Представляет собой строку длиной от 1 до 128 символов из набора [a-z, A-Z, 0-9, _, -, .]. Может начинаться с любого символа из набора, кроме точки.
        /// </summary>
        public string record_id;

        /// <summary>
        /// Изменения отдельных полей записи.
        /// </summary>
        public List<YaFieldChange> changes;
    }

    /// <summary>
    /// Изменение отдельного поля записи.
    /// </summary>
    public class YaFieldChange
    {
        /// <summary>
        /// Тип изменения, применяемого к полю записи (подробнее см. в разделе Типы изменений). https://tech.yandex.ru/datasync/http/doc/tasks/create-changes-docpage/#create-changes__changes-types
        /// </summary>
        public string change_type;

        /// <summary>
        /// Идентификатор поля. Представляет собой строку длиной от 1 до 128 символов из набора [a-z, A-Z, 0-9, _, -, .]. Может начинаться с любого символа из набора, кроме точки.
        /// </summary>
        public string field_id;

        /// <summary>
        /// Индекс элемента списка, к которому применяется изменение. Данное поле указывается при любом изменении, начинающегося с "list_item".
        /// </summary>
        public int list_item;

        /// <summary>
        /// Новый индекс элемента списка. Указывается при типе изменения list_item_move.
        /// </summary>
        public int list_item_dest;

        /// <summary>
        /// JSON-объект, содержащий данные, которые необходимо синхронизировать.
        /// </summary>
        public YaValue value;
    }
}