﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using Microsoft.Win32;
using System.Windows.Forms;
using System.IO;

namespace Greed.Core
{
	public struct NativeMacros
	{
		/// <summary>
		/// C++ макрос для GET_X_LPARAM.
		/// </summary>
		public static int GET_X_LPARAM(int x)
		{
			return x & 0xffff;
		}

		/// <summary>
		/// C++ макрос для GET_Y_LPARAM.
		/// </summary>
		public static int GET_Y_LPARAM(int x)
		{
			return (x >> 16) & 0xffff;
		}

		/// <summary>
		/// C++ макрос для MAKELONG.
		/// </summary>
		public static int MAKELONG(int x, int y)
		{
			return (x & 0xffff) | ((y & 0xffff) << 16);
		}
	}

	public static class Hooker
	{
        private static Random _random = new Random();

		public static List<int> GetHandlersByName(string name)
		{
			var hl = new List<int>();
			var h = User32Class.FindWindow(null, name);
			while (h != 0)
			{
				hl.Add(h);
				h = User32Class.FindWindowEx(0, h, null, name);
			}
			return hl;
		}

		public static List<int> GetHandlersByType(string type)
		{
			var hl = new List<int>();
			var h = User32Class.FindWindow(type, null);
			while (h != 0)
			{
				hl.Add(h);
				h = User32Class.FindWindowEx(0, h, type, null);
			}
			return hl;
		}

		public static List<int> GetHandlers(string name, string type)
		{
			return GetHandlersByName(name).Intersect(GetHandlersByType(type)).ToList();
		}

        public static int GetHandler(string name, string type)
        {
            var hl = GetHandlersByName(name).Intersect(GetHandlersByType(type)).ToList();
            return hl.Any() ? hl[0] : 0;
        }

        public static Point LastRectDeff;

		public static Rectangle GetHwndRect(int hwnd)
		{
			if (hwnd > 0)
			{
                Rectangle res1;
                Rectangle res2;
                User32Class.GetWindowRect(hwnd, out res1);
                User32Class.GetClientRect(hwnd, out res2);
                var w = res1.Width - res1.X;
				var b = (w - res2.Width)/2;
                var b2 = (res1.Height - res1.Y - res2.Height - b);

                LastRectDeff = new Point(b, b2);

				return new Rectangle
				{
					X = res1.X + b,
					Y = res1.Y + b2,
					Height = res2.Height,
					Width = res2.Width
				};
			}
			return new Rectangle();
		}

		public static Bitmap ImgScreen = null;
		private static Graphics _Graphics = null;
		private static int _lstImgScreenW = 0;
		private static int _lstImgScreenH = 0;

		public static Bitmap InitGraphics(int w, int h) {
			_lstImgScreenW = w;
			_lstImgScreenH = h;

			ImgScreen = new Bitmap(w, h);

			_Graphics = Graphics.FromImage(ImgScreen);

			return ImgScreen;
		}

		public static Bitmap GetRectScreen(Rectangle rect)
		{
            if (_Graphics == null || _lstImgScreenW != rect.Width || _lstImgScreenH != rect.Height)
            {
                InitGraphics(rect.Width, rect.Height);
            }

            _Graphics.CopyFromScreen(rect.Left, rect.Top, 0, 0, rect.Size);

            return ImgScreen;
        }

        public static byte[] GetRectScreenArr(Rectangle rect1)
        {
            Bitmap bmp = GetRectScreen(rect1);

            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadOnly, bmp.PixelFormat);

            IntPtr ptr = bmpData.Scan0;

            int bytes = bmpData.Stride * bmp.Height;
            byte[] rgbValues = new byte[bytes];

            Marshal.Copy(ptr, rgbValues, 0, bytes);

            bmp.UnlockBits(bmpData);

            return rgbValues;
        }

        public static void Resize(int hwnd, Rectangle rect)
        {
            User32Class.SetWindowPos(hwnd, 0, rect.X, rect.Y, rect.Width, rect.Height, 0);
        }

		public static void MouseMove(int hwnd, int x, int y, bool refCor = true)
		{
            var r = GetHwndRect(hwnd);
            //var wmNchittest = 0x0084;
            //User32Class.SendMessage(hwnd, wmNchittest, 0, NativeMacros.MAKELONG(r.X + x, r.Y + y));
            //var wmSetcursor = 0x20;
            //User32Class.SendMessage(hwnd, wmSetcursor, 0x000908B6, 0x02000001);
            //var wmNcmousemove = 0x00A0;
            //User32Class.SendMessage(hwnd, wmNcmousemove, 0, NativeMacros.MAKELONG(r.X + x, r.Y + y));
            Point cr = new Point(0, 0);
            if (refCor)
            {
                User32Class.GetCursorPos(ref cr);
            }
            
            var x1 = r.X + x;
            var y1 = r.Y + y;
            User32Class.SetCursorPos(x1, y1);
            //var lClip = Cursor.Clip;
            //Cursor.Clip = new Rectangle(x1, y1, 0, 0);
            //User32Class.SendMessage(hwnd, (int)Events.WmMousemove, 0, NativeMacros.MAKELONG(x, y));

            if (refCor)
            {
                Thread.Sleep(_random.Next(50, 100));
                //Cursor.Clip = lClip;
                User32Class.SetCursorPos(cr.X, cr.Y);
            }
		}

        public static void MouseClick(int hwnd, int x, int y, bool right, int minSleep = 25)
        {
            var r = GetHwndRect(hwnd);
            var cr = new Point(0, 0);
            User32Class.GetCursorPos(ref cr);
            var x1 = r.X + x;
            var y1 = r.Y + y;
            User32Class.SetCursorPos(x1, y1);
            Thread.Sleep(_random.Next(minSleep, minSleep + 25));
            User32Class.SendMessage(hwnd, 0x0204, 0x00000002, NativeMacros.MAKELONG(x1, y1));
            Thread.Sleep(_random.Next(100, 150));
            User32Class.SendMessage(hwnd, 0x0205, 0, NativeMacros.MAKELONG(x1, y1));
            Thread.Sleep(_random.Next(minSleep, minSleep + 25));
            User32Class.SetCursorPos(cr.X, cr.Y);
        }

		public static void SendKey(int hwnd, int k)
		{
			if (hwnd > 0)
			{
				User32Class.SendMessage(hwnd, 0x0100 /*WM_KEYDOWN*/, k, 0);
				Thread.Sleep(_random.Next(25, 50));
				User32Class.SendMessage(hwnd, 0x0101 /*WM_KEYUP*/, k, 0);
			}
		}

        public static void SendChar(int hwnd, int k, int m = 0)
        {
            {
                User32Class.SendMessage(hwnd, 0x0100 /*WM_KEYDOWN*/, k, m);
                User32Class.SendMessage(hwnd, 0x0102 /*WM_CHAR*/, k, m);
                User32Class.SendMessage(hwnd, 0x0101 /*WM_KEYUP*/, k, m);
            }
        }

        public static bool UpdateDPI()
        {
            try
            {
                return User32Class.SetProcessDpiAwareness(User32Class.PROCESS_DPI_AWARENESS.Process_Per_Monitor_DPI_Aware);
            }
            catch
            {

            }

            return false;
        }

        public static bool SetAutorunValue(string name, bool autorun)
        {
            var ExePath = Path.GetFullPath(Application.ExecutablePath);
            var reg = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run\\");
            try
            {
                var oldValue = reg.GetValue(name);
                if (autorun)
                {
                    if (oldValue == null)
                    {
                        reg.SetValue(name, ExePath);
                    }
                }
                else
                {
                    if (oldValue != null)
                    {
                        reg.DeleteValue(name);
                    }
                }

                reg.Close();
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}