﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;

namespace Greed.Core
{
	[Flags]
	public enum Events
	{
		WmMousemove = 0x0200,
		WmNcMousemove = 0x00A0
	}

	public class User32Class
	{
        public const int WmKeyDown = 0x100;
        public const int WmKeyUp = 0x101;
        public const int WmChar = 0x102;
        public const int MouseeventfLeftdown = 0x02;
        public const int MouseeventfLeftup = 0x04;
        public const int MouseeventfRightdown = 0x08;
        public const int MouseeventfRightup = 0x10;
        public const int WmMousemove = 0x0200;

		#region user32.dll

		[DllImport("user32.dll")]
		public static extern int FindWindow(string lpClassName, string lpWindowName);

		[DllImport("user32.dll")]
		public static extern int FindWindowEx(int hwndParent, int hwndChildAfter, string lpClassName, string lpWindowName);

		[DllImport("user32.dll")]
		public static extern IntPtr SendMessage(int hWnd, int msg, int wParam, int lParam);

		[DllImport("user32.dll")]
		public static extern int GetWindowText(int hwnd, StringBuilder buf, int nMaxCount);

		[DllImport("user32.dll")]
		public static extern int GetFocus();

		[DllImport("user32.dll")]
		public static extern int GetForegroundWindow();

		[DllImport("user32.dll")]
		public static extern int GetWindowThreadProcessId(int hWnd, out int processId);

		[DllImport("kernel32")]
		public static extern int GetCurrentThreadId();

		[DllImport("user32.dll")]
		public static extern int AttachThreadInput(int id, int owner, bool attach);

		[DllImport("user32.dll")]
		public static extern bool SetCursorPos(int x, int y);

		[DllImport("user32.dll")]
		public static extern bool GetCursorPos(ref Point lp);

		[DllImport("user32.dll")]
		public static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        [DllImport("user32.dll")]
        public static extern bool ClipCursor(Rectangle lpRect);

        //[DllImport("user32.dll")]
        //public static bool ClipCursor(Rectangle lpRect);

		[DllImport("user32.dll")]
		public static extern bool GetWindowRect(int hwnd, out Rectangle lpRect);

		[DllImport("user32.dll")]
		public static extern bool GetClientRect(int hwnd, out Rectangle lpRect);

		[DllImport("gdi32.dll")]
		public static extern uint GetPixel(int hwnd, int x, int y);

		[DllImport("user32.dll")]
		public static extern int GetDC(int hwnd);

		[DllImport("user32.dll")]
		public static extern int ReleaseDC(int hwnd, int hDC);

        [DllImport("user32.dll")]
        public static extern IntPtr SetWindowPos(int hWnd, int hWndInsertAfter, int  X, int  Y, int  cx, int  cy, int uFlags);

        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(int hWnd, int id, int fsMods, int vk);

        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(int hWnd, int id);

		#endregion user32.dll

        #region kernel32.dll

        [DllImport("kernel32.dll")]
		public static extern int GlobalAddAtomA(string name);
        
        [DllImport("kernel32.dll")]
        public static extern int GlobalDeleteAtom(int atom);

        #endregion kernel32.dll

        #region SHCore.dll

        [DllImport("SHCore.dll", SetLastError = true)]
        public static extern bool SetProcessDpiAwareness(PROCESS_DPI_AWARENESS awareness);

        [DllImport("SHCore.dll", SetLastError = true)]
        public static extern void GetProcessDpiAwareness(IntPtr hprocess, out PROCESS_DPI_AWARENESS awareness);

        public enum PROCESS_DPI_AWARENESS
        {
            Process_DPI_Unaware = 0,
            Process_System_DPI_Aware = 1,
            Process_Per_Monitor_DPI_Aware = 2
        }

        #endregion SHCore.dll
    }
}