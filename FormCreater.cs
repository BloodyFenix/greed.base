﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Greed.Core
{
    public class GreedForm: Form
    {
        public Dictionary<string, Control> MapEditor = new Dictionary<string, Control>();
    }

    public static class FormCreater
    {
        public static GreedForm Create(FormConfig config)
        {
            var res = new GreedForm
            {
                Icon = config.MainForm.Icon,
                Text = config.Title,
                Name = config.Key                
            };

            FormConfig.AddItems(res, config.Items, res.Controls);

            res.FormClosing += new FormClosingEventHandler((obj, args) =>
            {
                if (config.OnBeforeSave != null) config.OnBeforeSave();

                FormParams.Get(res, config.MainForm, Settings.GetDeltaMain).Save(config.Key);

                if (config.OnAfterSave != null) config.OnAfterSave();
            });
            res.Load += new EventHandler((obj, args) =>
            {
                if (config.OnBeforeLoad != null) config.OnBeforeLoad();

                FormParams.Load(config.Key).Set(res, config.MainForm, Settings.SetDeltaMain);

                if (config.OnAfterLoad != null) config.OnAfterLoad();
            });
            
            return res;
        }
    }
}
