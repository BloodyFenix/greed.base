﻿using System.IO;
using System.Xml.Serialization;

namespace Greed.Core
{
	public static class Serialization
	{
		public static void SerializeToFile<T>(this T t, string path) where T : class
		{
			using (Stream writer = new FileStream(path, FileMode.Create))
			{
				var serializer = new XmlSerializer(typeof(T));
				serializer.Serialize(writer, t);
			}
		}

		public static T DeserializeFromFile<T>(string path) where T : class
		{
			using (Stream stream = new FileStream(path, FileMode.Open))
			{
				var serializer = new XmlSerializer(typeof (T));
				return (T) serializer.Deserialize(stream);
			}
		}
	}
}