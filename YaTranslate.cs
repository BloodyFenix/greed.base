﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace Greed.Core
{
    public static class YaTranslate
    {
        private const string _Token = "t1.9euelZqPy5aMk8qTypuPiY6Sks3Gke3rnpWaz4-ax5HMiZ6MkJONxsrPm8fl9Pd2cFp3-e95HS6L3fT3Nh9Yd_nveR0uiw.fI7RCba1uQ4VM-MGlrctU5jv-Kl2ghLzrUxiwuCVukwspPPkgJreRLWmEmRelY58Ui2MGOB2Caa0kGhCNZ6vBA";
        private const string _FolderId = "b1gdr765qkru9lq53r3f";
        private const string _RequestTemplate = "{\"folderId\": \"{0}\", \"texts\": {1}, \"targetLanguageCode\": \"{2}\", \"sourceLanguageCode\": \"{3}\"}";

        private static string Ajax(string url, string method = "GET", string data = null, List<string> headers = null)
        {
            WebResponse response;
            return Ajax(url, out response, method, data, headers);
        }

        private static string Ajax(
            string url, out WebResponse response, string method = "GET", string data = null, List<string> headers = null
        )
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.Headers.Add("Authorization: Bearer " + _Token);

            if (headers != null) headers.ForEach(req.Headers.Add);

            req.ContentType = "application/json";

            req.Method = method;

            if (data != null)
            {
                byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(data);
                req.ContentLength = byteArray.Length;

                //записываем данные в поток запроса
                using (Stream dataStream = req.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                }
            }

            try
            {
                response = req.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream());
                return sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            response = null;
            return null;
        }

        /// <summary>
        /// Получение перевода
        /// https://cloud.yandex.ru/docs/translate/operations/translate
        /// </summary>
        /// <param name="texts">Список текстов для перевода</param>
        /// <param name="targetLanguageCode">Направление перевода</param>
        /// <param name="sourceLanguageCode">Язык исходного текста</param>
        public static YaTranslateResult GetTranslate(List<string> texts, string targetLanguageCode = "ru", string sourceLanguageCode = "en")
        {

            return JsonConvert.DeserializeObject<YaTranslateResult>(
                Ajax(
                    "https://translate.api.cloud.yandex.net/translate/v2/translate",
                    "POST",
                    _RequestTemplate
                        .Replace("{0}", _FolderId)
                        .Replace("{1}", JsonConvert.SerializeObject(texts))
                        .Replace("{2}", targetLanguageCode)
                        .Replace("{3}", sourceLanguageCode)
                )
            );
        }
    }

    public class YaTranslateResultText
    {
        public string text;
        public string detectedLanguageCode;
    }

    public class YaTranslateResult
    {
        public List<YaTranslateResultText> translations;
    }
}