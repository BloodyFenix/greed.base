﻿using System;
using System.Drawing;

namespace Greed.Core
{
	public class PartImg
	{
		public Bitmap Img;

		public int X;
		public int Y;
		public int W;
		public int H;

		public PartImg(Bitmap img)
		{
			Img = img;
			X = 0;
			Y = 0;
			W = img.Width;
			H = img.Height;
		}

		public PartImg(Bitmap img, int x, int y, int w, int h)
		{
			Img = img;
			X = x;
			Y = y;
			W = w;
			H = h;
		}

		public int[][] GetMap(int cLvl = 1)
		{
			if (cLvl == 1) return new[] {ImgCompare.MidBytes(this)};

			var res = new int[cLvl*cLvl][];
			var i = 0;
			var dx = ((double)W)/cLvl;
			var dy = ((double)H)/cLvl;
			for (int x = 0; x < cLvl; x++)
				for (int y = 0; y < cLvl; y++)
				{
                    var pi = new PartImg(Img, X + (int)Math.Round(dx * x), Y + (int)Math.Round(dy * y), (int)Math.Round(dx), (int)Math.Round(dy));
					res[i++] = ImgCompare.MidBytes(pi);
				}
			return res;
		}

        public int[] GetGrayMap(int cLvl = 8)
        {
            var rgbMap = GetMap(cLvl);
            var len = cLvl * cLvl;
            var res = new int[len];
            
            for (int i = 0; i < len; i++)
            {
                var rgb = rgbMap[i];
                res[i] = (rgb[0] + rgb[1] + rgb[2]) / 3;
            }

            return res;
        }
    }

	public static class ImgCompare
	{
		/// <summary>
		/// Сравнение изображение по среднему цвету
		/// </summary>
		/// <param name="img1"></param>
		/// <param name="img2"></param>
		/// <param name="cLvl">уровень вложенного сравнения(
		/// 	1 - сравниваются как есть;
		/// 	2 - деляться на 4 куска и сравнивается каждый;
		/// 	3 - 9 кусков
		/// ...)</param>
		/// <param name="limit">Допустимая погрешность</param>
		/// <param name="colorStep">Шаг для вычесления среднего цвета (1 - берется каждый пиксель; 2 - через 1 пиксель)</param>
		/// <returns></returns>
		public static bool Compare(PartImg img1, PartImg img2, int cLvl = 1, float limit = 0.05f, int colorStep = 1)
		{
			if (cLvl == 1) return ColorCompare(MidColor(img1, colorStep), MidColor(img2, colorStep), limit);

			var dx1 = (img1.W - img1.X)/cLvl;
			var dy1 = (img1.H - img1.Y)/cLvl;
			var dx2 = (img2.W - img2.X)/cLvl;
			var dy2 = (img2.H - img2.Y)/cLvl;
			for (int x = 0; x < cLvl; x++)
				for (int y = 0; y < cLvl; y++)
				{
					var pi1 = new PartImg(img1.Img, img1.X + dx1*x, img1.Y + dy1*y, img1.X + dx1*(x + 1), img1.Y + dy1*(y + 1));
					var pi2 = new PartImg(img2.Img, img2.X + dx2*x, img2.Y + dy2*y, img2.X + dx2*(x + 1), img2.Y + dy2*(y + 1));
					if (!ColorCompare(MidColor(pi1, colorStep), MidColor(pi2, colorStep), limit)) return false;
				}
			return true;
		}

		/// <summary>
		/// Сравнение цветов
		/// </summary>
		/// <param name="c1"></param>
		/// <param name="c2"></param>
		/// <param name="limit">Допустимая погрешность</param>
		/// <returns></returns>
		public static bool ColorCompare(Color c1, Color c2, float limit = 0.05f)
		{
			var v1 = c1.R;
			var v2 = c2.R;
			if ((v1 - v1*limit) >= v2 || (v1 + v1*limit) <= v2) return false;
			v1 = c1.G;
			v2 = c2.G;
			if ((v1 - v1*limit) >= v2 || (v1 + v1*limit) <= v2) return false;
			v1 = c1.B;
			v2 = c2.B;
			if ((v1 - v1*limit) >= v2 || (v1 + v1*limit) <= v2) return false;
			return true;
		}

		/// <summary>
		/// Сравнение цветов
		/// </summary>
		/// <param name="c1"></param>
		/// <param name="c2"></param>
		/// <param name="limit">Допустимая погрешность</param>
		/// <returns></returns>
		public static bool MapCompare(int[] c1, int[] c2, float limit = 0.05f)
		{
			for (var i = 0; i < 3; i++)
			{
				var v1 = c1[i];
				var v2 = c2[i];
				var dv = v1*limit;
				if (v1 - dv >= v2 || v1 + dv <= v2) return false;
			}

			return true;
		}

		/// <summary>
		/// Средний цвет изображения
		/// </summary>
		/// <param name="pi"></param>
		/// <param name="colorStep">Шаг для вычесления среднего цвета (1 - берется каждый пиксель; 2 - через 1 пиксель)</param>
		/// <returns></returns>
		public static Color MidColor(PartImg pi, int colorStep = 1)
		{
			long r = 0;
			long g = 0;
			long b = 0;
			var w = pi.W + pi.X;
			var h = pi.H + pi.Y;
			var i = 0;
			for (int x = pi.X; x < w; x += colorStep)
				for (int y = pi.Y; y < h; y += colorStep)
				{
					i++;
					var p = pi.Img.GetPixel(x, y);
					r += p.R;
					g += p.G;
					b += p.B;
				}
			return Color.FromArgb(1, (int) (r/i), (int) (g/i), (int) (b/i));
		}

		public static int[] MidBytes(PartImg pi, int colorStep = 1)
		{
			var c = MidColor(pi, colorStep);
			return new int[] {c.R, c.G, c.B};
		}
	}
}