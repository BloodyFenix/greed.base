﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Greed.Core
{
    public class FormConfig
    {
        public Form MainForm;
        public string Title;
        public string Key;

        public List<FormItemConfig> Items;

        public Action OnBeforeSave = null;
        public Action OnBeforeLoad = null;
        public Action OnAfterSave = null;
        public Action OnAfterLoad = null;

        public static void AddItems(GreedForm form, List<FormItemConfig> items, Control.ControlCollection Controls, bool Verticale = false)
        {
            var diff = 5;
            items.ForEach((itemConf) =>
            {
                diff += 5;
            
                var editor = itemConf.CreateEditor(form);

                if (editor != null)
                {
                    form.MapEditor[itemConf.GetKey()] = editor;

                    if (Verticale)
                    {
                        editor.Left = diff;
                        editor.Top = 15;
                    }
                    else
                    {
                        editor.Left = 15;
                        editor.Top = diff;
                    }

                    Controls.Add(editor);

                    if (!itemConf.GetNoLabel())
                    {
                        var label = new Label
                        {
                            Text = itemConf.GetLabel()
                        };

                        if (Verticale)
                        {
                            label.Top = 15 + editor.Bottom;
                            label.Left = diff + 2;
                        }
                        else
                        {
                            label.Left = 15 + editor.Right;
                            label.Top = diff + 2;
                        }

                        Controls.Add(label);
                    }

                    if (Verticale)
                    {
                        diff += editor.Width;
                    }
                    else {
                        diff += editor.Height;
                    }
                    
                }
            });
        }
    }

    public class FormItemConfig
    {
        public const string CheckBoxType = "CheckBox";
        public const string NumericType = "Numeric";
        public const string FieldSetType = "FieldSet";
        public const string LabelType = "Label";
        public const string ButtonType = "Button";

        public string Type;
        public string Label;
        public string Key;
        public object DefaultValue;
        public bool NoLabel;
        public int W;
        public int H;

        public virtual int GetW()
        {
            return W;
        }

        public virtual int GetH()
        {
            return H;
        }

        public virtual Control UpdEditor(Control editor)
        {
            if (editor == null) return editor;

            if (GetW() > 0)
            {
                editor.Width = GetW();
            }

            if (GetH() > 0)
            {
                editor.Height = GetH();
            }

            return editor;
        }

        public virtual Control CreateEditor(GreedForm form)
        {
            Control res = null;
            switch (GetType())
            {
                case FormItemConfig.CheckBoxType:
                    res = ((CheckBoxFormItemConfig)this).CreateEditor(form);
                    break;
                case FormItemConfig.FieldSetType:
                    res = ((FieldSetFormItemConfig)this).CreateEditor(form);
                    break;
            }

            return UpdEditor(res);
        }

        public virtual string GetType()
        {
            return Type;
        }

        public virtual string GetLabel()
        {
            return Label;
        }

        public virtual string GetKey()
        {
            return Key;
        }

        public virtual object GetDefaultValue()
        {
            return DefaultValue;
        }

        public virtual bool GetNoLabel()
        {
            return NoLabel;
        }
    }

    public class CheckBoxFormItemConfig: FormItemConfig
    {
        public CheckBox CheckBox = null;
        public EventHandler CheckedChanged = null;
        public bool SetSettingMode = true;
        public bool Checked = false;

        public override string GetType()
        {
            return CheckBoxType;
        }

        public override bool GetNoLabel()
        {
            return true;
        }

        public override Control CreateEditor(GreedForm form)
        {
            CheckBox = new CheckBox
            {
                Name = "cb" + GetKey(),
                Checked = SetSettingMode ? Settings.Get(GetKey(), (bool)GetDefaultValue()) : Checked,
                Text = GetLabel()
            };

            if (SetSettingMode)
            {
                CheckBox.CheckedChanged += (obj, args) =>
                {
                    Settings.Set(GetKey(), CheckBox.Checked);
                };
            }
            
            if (CheckedChanged != null)
            {
                CheckBox.CheckedChanged += CheckedChanged;
            }

            return UpdEditor(CheckBox);
        }
    }

    public class FieldSetFormItemConfig : FormItemConfig
    {
        public override string GetType()
        {
            return FormItemConfig.FieldSetType;
        }

        public override bool GetNoLabel()
        {
            return true;
        }

        public List<FormItemConfig> Items;
        public bool Verticale = true;

        public virtual List<FormItemConfig> GetItems()
        {
            return Items;
        }

        public override Control CreateEditor(GreedForm form)
        {
            var res = new GroupBox
            {
                Name = "fs" + this.GetKey(),
                Text = this.GetLabel()
            };

            var items = GetItems();
            if (items != null)
            {
                FormConfig.AddItems(form, items, res.Controls, Verticale);
            }

            return UpdEditor(res);
        }
    }

    public class LabelFormItemConfig : FormItemConfig
    {
        public override string GetType()
        {
            return FormItemConfig.LabelType;
        }

        public override bool GetNoLabel()
        {
            return true;
        }

        public override Control CreateEditor(GreedForm form)
        {
            var res = new Label
            {
                Name = "lbl" + this.GetKey(),
                Text = this.GetLabel()
            };

            return UpdEditor(res);
        }
    }

    public class NumericFormItemConfig : FormItemConfig
    {
        public override string GetType()
        {
            return FormItemConfig.NumericType;
        }

        public int Max = 0;
        public int Min = 0;
        public decimal Inc = 0;
        
        public override Control CreateEditor(GreedForm form)
        {
            var res = new NumericUpDown
            {
                Name = "num" + GetKey()
            };

            res.ValueChanged += (obj, args) =>
            {
                Settings.Set(GetKey(), res.Value);
            };

            if (Max != 0 || Min != 0)
            {
                res.Maximum = Max;
                res.Minimum = Min;
            }

            res.Value = Settings.Get<long>(GetKey(), (long)GetDefaultValue());

            if (Inc != 0) res.Increment = Inc;

            return UpdEditor(res);
        }
    }

    public class ButtonFormItemConfig : FormItemConfig
    {
        public override string GetType()
        {
            return FormItemConfig.ButtonType;
        }

        public override bool GetNoLabel()
        {
            return true;
        }

        public Action Action = null;

        public override Control CreateEditor(GreedForm form)
        {
            var res = new Button
            {
                Name = "btn" + GetKey(),
                Text = GetLabel()
            };

            res.Click += (obj, args) =>
            {
                if (Action != null) Action();
            };

            return UpdEditor(res);
        }
    }
}
