﻿using System;
using System.Threading;
using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Types;
using Telegram.Bot.Polling;
using System.Collections.Generic;

namespace Greed.Core
{
    public class TelegramBot
    {
        public ITelegramBotClient Bot;

        public TelegramBot(
            string token,
            long chatId,
            Dictionary<string, Action<ITelegramBotClient, Message, CancellationToken>> rules,
            Action<ITelegramBotClient, ApiRequestException, CancellationToken> exeption = null
        )
        {
            Init(token, chatId, rules, exeption);
        }

        public void Init(
            string token,
            long chatId,
            Dictionary<string, Action<ITelegramBotClient, Message, CancellationToken>> rules,
            Action<ITelegramBotClient, ApiRequestException, CancellationToken> exeption = null
        )
        {
            Bot = new TelegramBotClient(token);

            var cts = new CancellationTokenSource();
            var cancellationToken = cts.Token;
            // receive all update types
            var receiverOptions = new ReceiverOptions { AllowedUpdates = { } };
            Bot.StartReceiving(
                // HandleUpdateAsync,
                (ITelegramBotClient botClient, Update update, CancellationToken cancellToken) =>
                {
                    if (update.Message is Message message)
                    {
                        if (message.Text == "/id")
                        {
                            botClient.SendTextMessageAsync(message.Chat, message.Chat.Id.ToString());
                            return;
                        }
                        if (message.Chat.Id == chatId)
                        {
                            if (message.Text != null && rules.ContainsKey(message.Text))
                            {
                                rules[message.Text](botClient, message, cancellToken);
                            }
                            else
                            {
                                if (rules.ContainsKey("default")) rules["default"](botClient, message, cancellToken);
                            }
                        }
                    }
                },
                // HandleErrorAsync,
                async (ITelegramBotClient botClient, Exception error, CancellationToken cancellToken) =>
                {
                    if (error is ApiRequestException apiRequestException)
                    {
                        if (exeption != null) {
                            exeption(botClient, apiRequestException, cancellToken);
                            await Bot.CloseAsync();
                            Init(token, chatId, rules, exeption);
                        }
                    }
                },
                receiverOptions,
                cancellationToken
            );
        }
    }
}